﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmIndividui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvCapi = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlCapi = new System.Windows.Forms.Panel();
            this.pnlAssassino = new System.Windows.Forms.Panel();
            this.lvAssassini = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvVittime = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlVittime = new System.Windows.Forms.Panel();
            this.cbTipologia = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btRicerca = new System.Windows.Forms.Button();
            this.btModifica = new System.Windows.Forms.Button();
            this.btElimina = new System.Windows.Forms.Button();
            this.btNuovo = new System.Windows.Forms.Button();
            this.pnlCapi.SuspendLayout();
            this.pnlAssassino.SuspendLayout();
            this.pnlVittime.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvCapi
            // 
            this.lvCapi.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvCapi.HideSelection = false;
            this.lvCapi.Location = new System.Drawing.Point(23, 14);
            this.lvCapi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvCapi.Name = "lvCapi";
            this.lvCapi.Size = new System.Drawing.Size(849, 382);
            this.lvCapi.TabIndex = 0;
            this.lvCapi.UseCompatibleStateImageBehavior = false;
            this.lvCapi.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NOME e COGNOME";
            this.columnHeader1.Width = 210;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "SESSO";
            this.columnHeader2.Width = 84;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "USERNAME";
            this.columnHeader3.Width = 134;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "PASSWORD";
            this.columnHeader4.Width = 90;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "RUOLO";
            this.columnHeader5.Width = 99;
            // 
            // pnlCapi
            // 
            this.pnlCapi.Controls.Add(this.lvCapi);
            this.pnlCapi.Location = new System.Drawing.Point(25, 124);
            this.pnlCapi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlCapi.Name = "pnlCapi";
            this.pnlCapi.Size = new System.Drawing.Size(884, 420);
            this.pnlCapi.TabIndex = 6;
            // 
            // pnlAssassino
            // 
            this.pnlAssassino.Controls.Add(this.lvAssassini);
            this.pnlAssassino.Location = new System.Drawing.Point(17, 124);
            this.pnlAssassino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAssassino.Name = "pnlAssassino";
            this.pnlAssassino.Size = new System.Drawing.Size(892, 420);
            this.pnlAssassino.TabIndex = 7;
            // 
            // lvAssassini
            // 
            this.lvAssassini.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.lvAssassini.HideSelection = false;
            this.lvAssassini.Location = new System.Drawing.Point(5, 0);
            this.lvAssassini.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvAssassini.Name = "lvAssassini";
            this.lvAssassini.Size = new System.Drawing.Size(876, 424);
            this.lvAssassini.TabIndex = 8;
            this.lvAssassini.UseCompatibleStateImageBehavior = false;
            this.lvAssassini.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "NOME e COGNOME";
            this.columnHeader12.Width = 152;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "SESSO";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "LUOGO NASCITA";
            this.columnHeader14.Width = 138;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "DATA ARRESTO";
            this.columnHeader15.Width = 108;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "ANNI IN PRIGIONE";
            this.columnHeader16.Width = 124;
            // 
            // lvVittime
            // 
            this.lvVittime.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.lvVittime.HideSelection = false;
            this.lvVittime.Location = new System.Drawing.Point(4, 4);
            this.lvVittime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvVittime.Name = "lvVittime";
            this.lvVittime.Size = new System.Drawing.Size(864, 349);
            this.lvVittime.TabIndex = 7;
            this.lvVittime.UseCompatibleStateImageBehavior = false;
            this.lvVittime.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "NOME e COGNOME";
            this.columnHeader6.Width = 138;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "SESSO";
            this.columnHeader7.Width = 48;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "DATA NASCITA";
            this.columnHeader8.Width = 100;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "DATA E ORA MORTE";
            this.columnHeader9.Width = 146;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "LUOGO MORTE";
            this.columnHeader10.Width = 120;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "TIPOLOGIA";
            this.columnHeader11.Width = 86;
            // 
            // pnlVittime
            // 
            this.pnlVittime.Controls.Add(this.lvVittime);
            this.pnlVittime.Location = new System.Drawing.Point(40, 135);
            this.pnlVittime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlVittime.Name = "pnlVittime";
            this.pnlVittime.Size = new System.Drawing.Size(884, 385);
            this.pnlVittime.TabIndex = 8;
            // 
            // cbTipologia
            // 
            this.cbTipologia.FormattingEnabled = true;
            this.cbTipologia.Location = new System.Drawing.Point(17, 76);
            this.cbTipologia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbTipologia.Name = "cbTipologia";
            this.cbTipologia.Size = new System.Drawing.Size(216, 24);
            this.cbTipologia.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(531, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "Effettua una ricerca in base alla tipologia di morte/assassinio";
            // 
            // btRicerca
            // 
            this.btRicerca.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.cerca;
            this.btRicerca.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btRicerca.Location = new System.Drawing.Point(275, 66);
            this.btRicerca.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btRicerca.Name = "btRicerca";
            this.btRicerca.Size = new System.Drawing.Size(37, 36);
            this.btRicerca.TabIndex = 42;
            this.btRicerca.UseVisualStyleBackColor = true;
            // 
            // btModifica
            // 
            this.btModifica.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.penna;
            this.btModifica.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btModifica.Location = new System.Drawing.Point(944, 218);
            this.btModifica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btModifica.Name = "btModifica";
            this.btModifica.Size = new System.Drawing.Size(85, 81);
            this.btModifica.TabIndex = 41;
            this.btModifica.UseVisualStyleBackColor = true;
            // 
            // btElimina
            // 
            this.btElimina.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.cestino;
            this.btElimina.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btElimina.Location = new System.Drawing.Point(944, 306);
            this.btElimina.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btElimina.Name = "btElimina";
            this.btElimina.Size = new System.Drawing.Size(85, 81);
            this.btElimina.TabIndex = 40;
            this.btElimina.UseVisualStyleBackColor = true;
            this.btElimina.Click += new System.EventHandler(this.btElimina_Click);
            // 
            // btNuovo
            // 
            this.btNuovo.BackColor = System.Drawing.Color.Transparent;
            this.btNuovo.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.inserisciDEF;
            this.btNuovo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btNuovo.Location = new System.Drawing.Point(944, 129);
            this.btNuovo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btNuovo.Name = "btNuovo";
            this.btNuovo.Size = new System.Drawing.Size(85, 81);
            this.btNuovo.TabIndex = 39;
            this.btNuovo.UseVisualStyleBackColor = false;
            this.btNuovo.Click += new System.EventHandler(this.btNuovo_Click);
            // 
            // FrmIndividui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 588);
            this.Controls.Add(this.btRicerca);
            this.Controls.Add(this.btModifica);
            this.Controls.Add(this.btElimina);
            this.Controls.Add(this.btNuovo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlAssassino);
            this.Controls.Add(this.pnlCapi);
            this.Controls.Add(this.pnlVittime);
            this.Controls.Add(this.cbTipologia);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmIndividui";
            this.Text = "FrmIndividui";
            this.Load += new System.EventHandler(this.FrmIndividui_Load);
            this.pnlCapi.ResumeLayout(false);
            this.pnlAssassino.ResumeLayout(false);
            this.pnlVittime.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvCapi;
        private System.Windows.Forms.Panel pnlCapi;
        private System.Windows.Forms.ListView lvVittime;
        private System.Windows.Forms.Panel pnlAssassino;
        private System.Windows.Forms.ListView lvAssassini;
        private System.Windows.Forms.Panel pnlVittime;
        private System.Windows.Forms.ComboBox cbTipologia;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btModifica;
        private System.Windows.Forms.Button btElimina;
        private System.Windows.Forms.Button btNuovo;
        private System.Windows.Forms.Button btRicerca;
    }
}