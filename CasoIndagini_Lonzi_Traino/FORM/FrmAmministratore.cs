﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmAmministratore : Form
    {
        public FrmAmministratore()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FrmIndividui frmIndividui = new FrmIndividui(true, false, false);
            frmIndividui.ShowDialog();
        }

        private void btCapoIndagine_Click(object sender, EventArgs e)
        {
            FrmIndividuo frmIndividuo = new FrmIndividuo(false, false, true);
            frmIndividuo.ShowDialog();

        }
       
        private void btPassaCaso_Click(object sender, EventArgs e)
        {
            FrmCasi frmCasi = new FrmCasi();
            frmCasi.ShowDialog();

        }
    }
}
