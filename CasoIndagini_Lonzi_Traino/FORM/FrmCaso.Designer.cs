﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmCaso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btCapoIndagine = new System.Windows.Forms.Button();
            this.btInserisciAssassino = new System.Windows.Forms.Button();
            this.btInserisciProve = new System.Windows.Forms.Button();
            this.btInserisciVittima = new System.Windows.Forms.Button();
            this.cbTipologia = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btSalvaCaso = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(493, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 33);
            this.label3.TabIndex = 7;
            this.label3.Text = "INSERISCI CAPO D\'INDAGINE";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(241, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 33);
            this.label1.TabIndex = 8;
            this.label1.Text = "INSERISCI ASSASSINO";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(493, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 33);
            this.label2.TabIndex = 9;
            this.label2.Text = "INSERISCI VITTIMA";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(241, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 22);
            this.label4.TabIndex = 10;
            this.label4.Text = "INSERISCI PROVE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btCapoIndagine
            // 
            this.btCapoIndagine.BackColor = System.Drawing.Color.White;
            this.btCapoIndagine.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.uomo;
            this.btCapoIndagine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCapoIndagine.Location = new System.Drawing.Point(530, 54);
            this.btCapoIndagine.Name = "btCapoIndagine";
            this.btCapoIndagine.Size = new System.Drawing.Size(95, 97);
            this.btCapoIndagine.TabIndex = 4;
            this.btCapoIndagine.UseVisualStyleBackColor = false;
            this.btCapoIndagine.Click += new System.EventHandler(this.btCapoIndagine_Click);
            // 
            // btInserisciAssassino
            // 
            this.btInserisciAssassino.BackColor = System.Drawing.Color.White;
            this.btInserisciAssassino.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.assassino;
            this.btInserisciAssassino.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btInserisciAssassino.Location = new System.Drawing.Point(267, 54);
            this.btInserisciAssassino.Name = "btInserisciAssassino";
            this.btInserisciAssassino.Size = new System.Drawing.Size(95, 97);
            this.btInserisciAssassino.TabIndex = 3;
            this.btInserisciAssassino.UseVisualStyleBackColor = false;
            this.btInserisciAssassino.Click += new System.EventHandler(this.btInserisciAssassino_Click);
            // 
            // btInserisciProve
            // 
            this.btInserisciProve.BackColor = System.Drawing.SystemColors.Control;
            this.btInserisciProve.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.prova;
            this.btInserisciProve.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btInserisciProve.Location = new System.Drawing.Point(267, 197);
            this.btInserisciProve.Name = "btInserisciProve";
            this.btInserisciProve.Size = new System.Drawing.Size(95, 97);
            this.btInserisciProve.TabIndex = 2;
            this.btInserisciProve.UseVisualStyleBackColor = false;
            this.btInserisciProve.Click += new System.EventHandler(this.btInserisciProve_Click);
            // 
            // btInserisciVittima
            // 
            this.btInserisciVittima.BackColor = System.Drawing.Color.White;
            this.btInserisciVittima.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.vittima;
            this.btInserisciVittima.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btInserisciVittima.Location = new System.Drawing.Point(530, 197);
            this.btInserisciVittima.Name = "btInserisciVittima";
            this.btInserisciVittima.Size = new System.Drawing.Size(95, 97);
            this.btInserisciVittima.TabIndex = 0;
            this.btInserisciVittima.UseVisualStyleBackColor = false;
            this.btInserisciVittima.Click += new System.EventHandler(this.btInserisciVittima_Click);
            // 
            // cbTipologia
            // 
            this.cbTipologia.FormattingEnabled = true;
            this.cbTipologia.Location = new System.Drawing.Point(43, 64);
            this.cbTipologia.Name = "cbTipologia";
            this.cbTipologia.Size = new System.Drawing.Size(121, 21);
            this.cbTipologia.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 43);
            this.label6.TabIndex = 13;
            this.label6.Text = "TIPOLOGIA CASO";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btSalvaCaso
            // 
            this.btSalvaCaso.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btSalvaCaso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btSalvaCaso.Location = new System.Drawing.Point(307, 322);
            this.btSalvaCaso.Name = "btSalvaCaso";
            this.btSalvaCaso.Size = new System.Drawing.Size(91, 52);
            this.btSalvaCaso.TabIndex = 36;
            this.btSalvaCaso.Text = "SALVA CASO";
            this.btSalvaCaso.UseVisualStyleBackColor = false;
            this.btSalvaCaso.Click += new System.EventHandler(this.btSalvaCaso_Click);
            // 
            // FrmCaso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(701, 386);
            this.Controls.Add(this.btSalvaCaso);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbTipologia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btCapoIndagine);
            this.Controls.Add(this.btInserisciAssassino);
            this.Controls.Add(this.btInserisciProve);
            this.Controls.Add(this.btInserisciVittima);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "FrmCaso";
            this.Text = "FrmCaso";
            this.Load += new System.EventHandler(this.FrmCaso_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btInserisciVittima;
        private System.Windows.Forms.Button btInserisciProve;
        private System.Windows.Forms.Button btInserisciAssassino;
        private System.Windows.Forms.Button btCapoIndagine;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbTipologia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btSalvaCaso;
    }
}