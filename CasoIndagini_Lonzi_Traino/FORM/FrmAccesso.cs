﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmAccesso : Form
    {
        public FrmAccesso()
        {
            InitializeComponent();
        }
        

        private void btLogin_Click(object sender, EventArgs e) //lonzi
        {
            //cerca se è amministratore  e all'interno controlla anche se sia capo visto che al suo interno c'è un richiamo alla procedura di verifica del capoIndagini 
            //se le username e password non corrispondono all'amministratore lonzi
            ClsAmministratoreBL.cercaAmministratore(tbUsername.Text, tbPassword.Text);

        }
        
    }
}
