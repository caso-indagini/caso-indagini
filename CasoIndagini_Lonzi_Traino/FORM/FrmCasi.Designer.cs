﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmCasi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cbRicercaTipologia = new System.Windows.Forms.ComboBox();
            this.btModifica = new System.Windows.Forms.Button();
            this.btElimina = new System.Windows.Forms.Button();
            this.btNuovoCaso = new System.Windows.Forms.Button();
            this.btRicerca = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(16, 87);
            this.listView1.Margin = new System.Windows.Forms.Padding(4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(416, 355);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "DATA";
            this.columnHeader1.Width = 144;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TIPOLOGIA";
            this.columnHeader2.Width = 143;
            // 
            // cbRicercaTipologia
            // 
            this.cbRicercaTipologia.FormattingEnabled = true;
            this.cbRicercaTipologia.Location = new System.Drawing.Point(16, 36);
            this.cbRicercaTipologia.Margin = new System.Windows.Forms.Padding(4);
            this.cbRicercaTipologia.Name = "cbRicercaTipologia";
            this.cbRicercaTipologia.Size = new System.Drawing.Size(284, 24);
            this.cbRicercaTipologia.TabIndex = 5;
            // 
            // btModifica
            // 
            this.btModifica.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.penna;
            this.btModifica.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btModifica.Location = new System.Drawing.Point(445, 177);
            this.btModifica.Margin = new System.Windows.Forms.Padding(4);
            this.btModifica.Name = "btModifica";
            this.btModifica.Size = new System.Drawing.Size(85, 81);
            this.btModifica.TabIndex = 4;
            this.btModifica.UseVisualStyleBackColor = true;
            // 
            // btElimina
            // 
            this.btElimina.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.cestino;
            this.btElimina.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btElimina.Location = new System.Drawing.Point(445, 266);
            this.btElimina.Margin = new System.Windows.Forms.Padding(4);
            this.btElimina.Name = "btElimina";
            this.btElimina.Size = new System.Drawing.Size(85, 81);
            this.btElimina.TabIndex = 3;
            this.btElimina.UseVisualStyleBackColor = true;
            // 
            // btNuovoCaso
            // 
            this.btNuovoCaso.BackColor = System.Drawing.Color.Transparent;
            this.btNuovoCaso.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.inserisciDEF;
            this.btNuovoCaso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btNuovoCaso.Location = new System.Drawing.Point(445, 89);
            this.btNuovoCaso.Margin = new System.Windows.Forms.Padding(4);
            this.btNuovoCaso.Name = "btNuovoCaso";
            this.btNuovoCaso.Size = new System.Drawing.Size(85, 81);
            this.btNuovoCaso.TabIndex = 2;
            this.btNuovoCaso.UseVisualStyleBackColor = false;
            this.btNuovoCaso.Click += new System.EventHandler(this.btNuovoCaso_Click);
            // 
            // btRicerca
            // 
            this.btRicerca.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.cerca;
            this.btRicerca.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btRicerca.Location = new System.Drawing.Point(309, 25);
            this.btRicerca.Margin = new System.Windows.Forms.Padding(4);
            this.btRicerca.Name = "btRicerca";
            this.btRicerca.Size = new System.Drawing.Size(44, 44);
            this.btRicerca.TabIndex = 1;
            this.btRicerca.UseVisualStyleBackColor = true;
            // 
            // FrmCasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 497);
            this.Controls.Add(this.cbRicercaTipologia);
            this.Controls.Add(this.btModifica);
            this.Controls.Add(this.btElimina);
            this.Controls.Add(this.btNuovoCaso);
            this.Controls.Add(this.btRicerca);
            this.Controls.Add(this.listView1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmCasi";
            this.Text = "FrmCasi";
            this.Load += new System.EventHandler(this.FrmCasi_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btRicerca;
        private System.Windows.Forms.Button btNuovoCaso;
        private System.Windows.Forms.Button btElimina;
        private System.Windows.Forms.ComboBox cbRicercaTipologia;
        public System.Windows.Forms.Button btModifica;
    }
}