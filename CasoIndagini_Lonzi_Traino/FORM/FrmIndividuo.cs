﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    
    public partial class FrmIndividuo : Form
    {
        bool _assassino,_vittima, _capo ;

        //bottone annulla
        private void btAnnulla_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //popolo la combobox per il sesso della persona
        private void popolaComboboxCitta()
        {
            foreach (Program.eCitta valore in Enum.GetValues(typeof(Program.eCitta)))
            {
                cbCittaNascita.Items.Add(valore.ToString());
                cbcLuogoNascita.Items.Add(valore.ToString());
                cbLuogoMorte.Items.Add(valore.ToString());
            }
        }


        // popola combobox delle città censite
        private void popolaComboboxSesso()
        {
            foreach (Program.eSesso valore in Enum.GetValues(typeof(Program.eSesso)))
            {
                cbSesso.Items.Add(valore.ToString());
            }
        }

        // salvataggio su database nella tabella persone
        private void salvataggioDatabasePersone()//lonzi
        {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO persone (Cf, nome, cognome, sesso) values (@Cf, @nome, @cognome, @sesso)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@Cf", tbCodiceFiscale.Text),
                new MySqlParameter("@nome", tbNome.Text),
                new MySqlParameter("@cognome", tbCognome.Text),
                new MySqlParameter("@sesso", cbSesso.SelectedItem)
                
            };


            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");

        }


        private void btSalva_Click(object sender, EventArgs e)
        {
            if (InserimentoCorretto())
            {
                if (pnlAssassino.Visible)
                {
                    //lonzi
                    #region salvataggio su database nella tabella assassino
                    //Vengono richiamati i metodi messi a disposizione dal DBManager
                    DBManager dbm = new DBManager();

                    string errore = "";

                    string sql = "INSERT INTO assassini (Cf, movente, giornoArresto, numeroAnniPrigione, nomecittaNascita) values (@Cf, @movente, @giornoArresto, @numeroAnniPrigione, @nomeCittaNascita)";

                    MySqlParameter[] parametri =
                    {
                        new MySqlParameter("@Cf", tbCodiceFiscale.Text),
                        new MySqlParameter("@movente", tbMovente.Text),
                        new MySqlParameter("@giornoArresto", mtbGiornoArresto.Text),
                        new MySqlParameter("@numeroAnniPrigione", tbNumAnniGalera.Text),
                        new MySqlParameter("@nomeCittaNascita", cbCittaNascita.SelectedItem)
                    };


                    long num = dbm.GetAffectedRows(sql, parametri, ref errore);

                    if (string.IsNullOrEmpty(errore))
                        MessageBox.Show($"LastID: {num}");
                    else
                        MessageBox.Show($"Errore: {errore}");
                    #endregion //lonzi

                    FrmIndividui frmAssassini = new FrmIndividui(false, true, false);
                    frmAssassini.ShowDialog();
                }
                else if (pnlCapo.Visible)
                {
                    //lonzi
                    #region  inserimento ruolo capo nella tabella RuoliCapo
                    DBManager dbm = new DBManager();

                    string errore = "";

                    string sql = "INSERT INTO ruoliCapo (Cf, ruolo) values (@Cf, @ruolo)";

                    MySqlParameter[] parametri =
                    {
                        new MySqlParameter("@Cf", tbCodiceFiscale.Text),
                        new MySqlParameter("@ruolo", tbRuolo.Text)
                        
                    };

                    long num = dbm.GetAffectedRows(sql, parametri, ref errore);

                    if (string.IsNullOrEmpty(errore))
                        MessageBox.Show($"LastID: {num}");
                    else
                        MessageBox.Show($"Errore: {errore}");
                    #endregion

                   //// #region  inserimento password e username 
                   // DBManager dbm2 = new DBManager();

                   // string errore2 = "";

                   // string sql2 = "INSERT INTO ruoliCapo (Cf, ruolo) values (@Cf, @ruolo)";

                   // MySqlParameter[] parametri2 =
                   // {
                   //     new MySqlParameter("@Cf", tbCodiceFiscale.Text),
                   //     new MySqlParameter("@ruolo", tbRuolo.Text)

                   // };

                   // long num2 = dbm.GetAffectedRows(sql, parametri, ref errore);

                   // if (string.IsNullOrEmpty(errore))
                   //     MessageBox.Show($"LastID: {num}");
                   // else
                   //     MessageBox.Show($"Errore: {errore}");
                   // #endregion

                    FrmIndividui frmCapi = new FrmIndividui(true, false, false);
                    frmCapi.ShowDialog();
                }
                else if (pnlVittima.Visible)
                {
                    //lonzi
                    #region  inserimento dati nella tabella vittime
                    //Vengono richiamati i metodi messi a disposizione dal DBManager
                    DBManager dbm = new DBManager();

                    string errore = "";

                    string sql = "INSERT INTO vittime (Cf, dataNascita, dataMorte, oraMorte, Idcaso, nomeCittaNascita, nomeCittaMorte) values (@Cf, @dataNascita, @dataMorte, @oraMorte, @Idcaso, @nomeCittaNascita, @nomeCittaMorte)";

                    MySqlParameter[] parametri =
                    {
                        new MySqlParameter("@Cf", tbCodiceFiscale.Text),
                        new MySqlParameter("@dataNascita", mtbDataNascita.Text),
                        new MySqlParameter("@dataMorte", mtbDataMorte.Text),
                        new MySqlParameter("@oraMorte", mtbOraMorte.Text),
                        //new MySqlParameter("@Idcaso", cbCittaNascita.SelectedItem)
                        new MySqlParameter("@nomeCittaNascita", cbcLuogoNascita.SelectedItem),
                        new MySqlParameter("@nomeCittaMorte", cbLuogoMorte.SelectedItem),
                    };


                    long num = dbm.GetAffectedRows(sql, parametri, ref errore);

                    if (string.IsNullOrEmpty(errore))
                        MessageBox.Show($"LastID: {num}");
                    else
                        MessageBox.Show($"Errore: {errore}");
                    #endregion


                   
                    FrmIndividui frmVittima = new FrmIndividui(true, false, false);
                    frmVittima.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("Errore!! Inserisci tutti i campi");
            }
            

        }
        bool InserimentoCorretto()//traino
        {
            if (tbCodiceFiscale.Text.Length < 1)
                return false;
            else if (tbNome.Text.Length < 1)
                return false;
            else if (tbCognome.Text.Length < 1)
                return false;
            else if (cbSesso.SelectedItem.ToString()=="")
                return false;

            if (pnlAssassino.Visible)
            {
                if (cbCittaNascita.SelectedItem.ToString() == "")
                    return false;
                else if (tbMovente.Text.Length < 1)
                    return false;
                else if (mtbGiornoArresto.Text.Length < 8)
                    return false;
                else if (tbNumAnniGalera.Text.Length < 8)
                    return false;
            }
            else if (pnlCapo.Visible)
            {
                if (tbRuolo.Text.Length < 8)
                    return false;
                else if (tbUsername.Text.Length < 1)
                    return false;
                else if (tbPassword.Text.Length < 8)
                    return false;
                
            }
            else if (pnlVittima.Visible)
            {
                if (mtbDataMorte.Text.Length < 8)
                    return false;
                else if (cbCittaNascita.SelectedItem.ToString() == "")
                    return false;
                else if (mtbDataNascita.Text.Length < 8)
                    return false;
                else if (mtbOraMorte.Text.Length < 8)
                    return false;
                else if (cbLuogoMorte.SelectedItem.ToString() == "")
                    return false;
            }

            return true;
        }

        

        public FrmIndividuo(bool assassino, bool vittima, bool capo)//traino
        {
            InitializeComponent();
            _assassino = assassino;
            _vittima = vittima;
            _capo = capo;
           
        }

        private void FrmIndividuo_Load(object sender, EventArgs e)//traino
        {
            popolaComboboxSesso();
            popolaComboboxCitta();

            if (_assassino)
            {
                pnlVittima.Visible = false;
                pnlCapo.Visible = false;
            }
            else if (_vittima)
            {
                pnlAssassino.Visible = false;
                pnlCapo.Visible = false;
            }
            else if (_capo)
            {
                pnlVittima.Visible = false;
                pnlAssassino.Visible = false;
                pnlCapo.Visible = true;
            }
        }
    }
}
