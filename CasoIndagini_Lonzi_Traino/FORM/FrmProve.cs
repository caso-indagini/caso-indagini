﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmProve : Form
    {
        public FrmProve()
        {
            InitializeComponent();
        }
        // salvataggio su database dei dati lonzi
         private void salvaSuDatabase()
         {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO prove (Descrizione, Tipologia) values (@Descrizione, @Tipologia)";

            MySqlParameter[] parametri =
            {
                        new MySqlParameter("@Descrizione", tbDescrizione.Text),
                        new MySqlParameter("@Tipologia", tbTipologia.Text),
                        
            };


            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");
         }
        // controllo se tutti i campi siano inseriti lonzi
        private void controllo()
        {
            if (tbTipologia.Text == "" || tbDescrizione.Text == "")
                MessageBox.Show("Compila tutti i campi!!");
        }

        private void btNuovaProva_Click(object sender, EventArgs e) // lonzi 
        {
            pnlProva.Visible = true;
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            controllo();
            salvaSuDatabase();
        } //lonzi

        private void btAnnulla_Click(object sender, EventArgs e)//lonzi
        {
            this.Close();
        }

        private void btEliminaProva_Click(object sender, EventArgs e)
        {
            DBManager dbm = new DBManager();

            string sql = "DELETE INTO persone (Cf, nome, cognome, sesso) values (@Cf, @nome, @cognome, @sesso)";
        }
    }
}
