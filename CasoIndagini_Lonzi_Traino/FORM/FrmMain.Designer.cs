﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.btLogOut = new System.Windows.Forms.Button();
            this.btEsci = new System.Windows.Forms.Button();
            this.btCasiVecchi = new System.Windows.Forms.Button();
            this.btNuovoCaso = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btLogOut
            // 
            this.btLogOut.BackColor = System.Drawing.Color.White;
            this.btLogOut.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.LogOut;
            this.btLogOut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btLogOut.Location = new System.Drawing.Point(333, 195);
            this.btLogOut.Name = "btLogOut";
            this.btLogOut.Size = new System.Drawing.Size(95, 97);
            this.btLogOut.TabIndex = 3;
            this.btLogOut.UseVisualStyleBackColor = false;
            this.btLogOut.Click += new System.EventHandler(this.btLogOut_Click);
            // 
            // btEsci
            // 
            this.btEsci.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.esci;
            this.btEsci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btEsci.Location = new System.Drawing.Point(59, 195);
            this.btEsci.Name = "btEsci";
            this.btEsci.Size = new System.Drawing.Size(95, 97);
            this.btEsci.TabIndex = 2;
            this.btEsci.UseVisualStyleBackColor = true;
            this.btEsci.Click += new System.EventHandler(this.btEsci_Click);
            // 
            // btCasiVecchi
            // 
            this.btCasiVecchi.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.guardacasi;
            this.btCasiVecchi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btCasiVecchi.Location = new System.Drawing.Point(333, 56);
            this.btCasiVecchi.Name = "btCasiVecchi";
            this.btCasiVecchi.Size = new System.Drawing.Size(95, 97);
            this.btCasiVecchi.TabIndex = 1;
            this.btCasiVecchi.UseVisualStyleBackColor = true;
            this.btCasiVecchi.Click += new System.EventHandler(this.btCasiVecchi_Click);
            // 
            // btNuovoCaso
            // 
            this.btNuovoCaso.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.aggiungi;
            this.btNuovoCaso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btNuovoCaso.Location = new System.Drawing.Point(59, 56);
            this.btNuovoCaso.Name = "btNuovoCaso";
            this.btNuovoCaso.Size = new System.Drawing.Size(95, 97);
            this.btNuovoCaso.TabIndex = 0;
            this.btNuovoCaso.UseVisualStyleBackColor = true;
            this.btNuovoCaso.Click += new System.EventHandler(this.btNuovoCaso_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(46, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "AGGIUNGI CASO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(305, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "GUARDA CASI VECCHI";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(84, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "ESCI";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(342, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "LOGOUT";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(501, 325);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btLogOut);
            this.Controls.Add(this.btEsci);
            this.Controls.Add(this.btCasiVecchi);
            this.Controls.Add(this.btNuovoCaso);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btNuovoCaso;
        private System.Windows.Forms.Button btCasiVecchi;
        private System.Windows.Forms.Button btEsci;
        private System.Windows.Forms.Button btLogOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

