﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySqlConnector;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmCaso : Form
    {
        public FrmCaso()
        {
            InitializeComponent();
        }
        // popolazione combobox tipologia caso lonzi 
        private void popolaCombobox()
        {
            foreach (Program.eTipologiaCaso valore in Enum.GetValues(typeof(Program.eTipologiaCaso)))
            {
                cbTipologia.Items.Add(valore.ToString());
            }
        }

        private void btInserisciAssassino_Click(object sender, EventArgs e)//traino
        {
            FrmIndividuo frmAssassini = new FrmIndividuo(true, false, false);
            frmAssassini.ShowDialog();
        }

        private void btInserisciVittima_Click(object sender, EventArgs e)//traino
        {
            FrmIndividuo frmVittima = new FrmIndividuo(false, true, false);
            frmVittima.ShowDialog();
        }

        private void btCapoIndagine_Click(object sender, EventArgs e)//traino
        {
            FrmIndividuo frmCapo = new FrmIndividuo(false, false, true);
            frmCapo.ShowDialog();
        }

        private void btInserisciProve_Click(object sender, EventArgs e)//traino
        {
            FrmProve frmProve = new FrmProve();
            frmProve.ShowDialog();
        }

        private void FrmCaso_Load(object sender, EventArgs e)
        {
            popolaCombobox();

            
           
        }

        private void btSalvaCaso_Click(object sender, EventArgs e)//lonzi
        {
            //Vengono richiamati i metodi messi a disposizione dal DBManager
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "INSERT INTO casi (isbn, autore, titolo, prezzo) values (@isbn, @autore, @titolo, @prezzo)";

            MySqlParameter[] parametri =
            {
                new MySqlParameter("@isbn", "1234"),
                new MySqlParameter("@autore", "Pinuccio"),
                new MySqlParameter("@titolo", "Sono"),
                new MySqlParameter("@prezzo", 0)
            };

            long num = dbm.GetAffectedRows(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
                MessageBox.Show($"LastID: {num}");
            else
                MessageBox.Show($"Errore: {errore}");

        }
    }
}
