﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmIndividuo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtbGiornoArresto = new System.Windows.Forms.MaskedTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btSalva = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.tbNumAnniGalera = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMovente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbCognome = new System.Windows.Forms.TextBox();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.tbCodiceFiscale = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlAssassino = new System.Windows.Forms.Panel();
            this.cbCittaNascita = new System.Windows.Forms.ComboBox();
            this.pnlCapo = new System.Windows.Forms.Panel();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbRuolo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlVittima = new System.Windows.Forms.Panel();
            this.cbLuogoMorte = new System.Windows.Forms.ComboBox();
            this.cbcLuogoNascita = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.mtbDataMorte = new System.Windows.Forms.MaskedTextBox();
            this.mtbDataNascita = new System.Windows.Forms.MaskedTextBox();
            this.mtbOraMorte = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbSesso = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlAssassino.SuspendLayout();
            this.pnlCapo.SuspendLayout();
            this.pnlVittima.SuspendLayout();
            this.SuspendLayout();
            // 
            // mtbGiornoArresto
            // 
            this.mtbGiornoArresto.Location = new System.Drawing.Point(144, 73);
            this.mtbGiornoArresto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbGiornoArresto.Mask = "00/00/0000";
            this.mtbGiornoArresto.Name = "mtbGiornoArresto";
            this.mtbGiornoArresto.Size = new System.Drawing.Size(132, 22);
            this.mtbGiornoArresto.TabIndex = 69;
            this.mtbGiornoArresto.ValidatingType = typeof(System.DateTime);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CasoIndagini_Lonzi_Traino.Properties.Resources.icona2;
            this.pictureBox1.Location = new System.Drawing.Point(16, 64);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 133);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 68;
            this.pictureBox1.TabStop = false;
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btSalva.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSalva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btSalva.Location = new System.Drawing.Point(380, 411);
            this.btSalva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(115, 53);
            this.btSalva.TabIndex = 67;
            this.btSalva.Text = "SALVA";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btAnnulla.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btAnnulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btAnnulla.Location = new System.Drawing.Point(237, 411);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(135, 53);
            this.btAnnulla.TabIndex = 66;
            this.btAnnulla.Text = "ANNULLA";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // tbNumAnniGalera
            // 
            this.tbNumAnniGalera.Location = new System.Drawing.Point(147, 105);
            this.tbNumAnniGalera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbNumAnniGalera.Name = "tbNumAnniGalera";
            this.tbNumAnniGalera.Size = new System.Drawing.Size(132, 22);
            this.tbNumAnniGalera.TabIndex = 65;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(-4, 108);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 17);
            this.label8.TabIndex = 64;
            this.label8.Text = "ANNI IN PRIGIONE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-4, 76);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 17);
            this.label7.TabIndex = 63;
            this.label7.Text = "GIORNO ARRESTO";
            // 
            // tbMovente
            // 
            this.tbMovente.Location = new System.Drawing.Point(147, 41);
            this.tbMovente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbMovente.Name = "tbMovente";
            this.tbMovente.Size = new System.Drawing.Size(132, 22);
            this.tbMovente.TabIndex = 62;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(-4, 44);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 61;
            this.label6.Text = "MOVENTE";
            // 
            // tbCognome
            // 
            this.tbCognome.Location = new System.Drawing.Point(364, 86);
            this.tbCognome.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCognome.Name = "tbCognome";
            this.tbCognome.Size = new System.Drawing.Size(132, 22);
            this.tbCognome.TabIndex = 58;
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(364, 54);
            this.tbNome.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(132, 22);
            this.tbNome.TabIndex = 57;
            // 
            // tbCodiceFiscale
            // 
            this.tbCodiceFiscale.Location = new System.Drawing.Point(364, 21);
            this.tbCodiceFiscale.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCodiceFiscale.Name = "tbCodiceFiscale";
            this.tbCodiceFiscale.Size = new System.Drawing.Size(132, 22);
            this.tbCodiceFiscale.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(-4, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 17);
            this.label5.TabIndex = 55;
            this.label5.Text = "LUOGO DI NASCITA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 17);
            this.label4.TabIndex = 54;
            this.label4.Text = "SESSO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 53;
            this.label3.Text = "COGNOME";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 58);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 52;
            this.label2.Text = "NOME";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 51;
            this.label1.Text = "CODICE FISCALE";
            // 
            // pnlAssassino
            // 
            this.pnlAssassino.Controls.Add(this.cbCittaNascita);
            this.pnlAssassino.Controls.Add(this.label5);
            this.pnlAssassino.Controls.Add(this.mtbGiornoArresto);
            this.pnlAssassino.Controls.Add(this.label6);
            this.pnlAssassino.Controls.Add(this.tbMovente);
            this.pnlAssassino.Controls.Add(this.label7);
            this.pnlAssassino.Controls.Add(this.tbNumAnniGalera);
            this.pnlAssassino.Controls.Add(this.label8);
            this.pnlAssassino.Location = new System.Drawing.Point(217, 148);
            this.pnlAssassino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAssassino.Name = "pnlAssassino";
            this.pnlAssassino.Size = new System.Drawing.Size(332, 139);
            this.pnlAssassino.TabIndex = 70;
            // 
            // cbCittaNascita
            // 
            this.cbCittaNascita.FormattingEnabled = true;
            this.cbCittaNascita.Location = new System.Drawing.Point(147, 1);
            this.cbCittaNascita.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbCittaNascita.Name = "cbCittaNascita";
            this.cbCittaNascita.Size = new System.Drawing.Size(132, 24);
            this.cbCittaNascita.TabIndex = 70;
            // 
            // pnlCapo
            // 
            this.pnlCapo.Controls.Add(this.tbPassword);
            this.pnlCapo.Controls.Add(this.label11);
            this.pnlCapo.Controls.Add(this.tbUsername);
            this.pnlCapo.Controls.Add(this.label10);
            this.pnlCapo.Controls.Add(this.tbRuolo);
            this.pnlCapo.Controls.Add(this.label9);
            this.pnlCapo.Location = new System.Drawing.Point(216, 148);
            this.pnlCapo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlCapo.Name = "pnlCapo";
            this.pnlCapo.Size = new System.Drawing.Size(332, 139);
            this.pnlCapo.TabIndex = 71;
            this.pnlCapo.Visible = false;
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(148, 68);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(132, 22);
            this.tbPassword.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(-3, 71);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "PASSWORD";
            // 
            // tbUsername
            // 
            this.tbUsername.Location = new System.Drawing.Point(148, 36);
            this.tbUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(132, 22);
            this.tbUsername.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(-3, 39);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "USERNAME";
            // 
            // tbRuolo
            // 
            this.tbRuolo.Location = new System.Drawing.Point(148, 4);
            this.tbRuolo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbRuolo.Name = "tbRuolo";
            this.tbRuolo.Size = new System.Drawing.Size(132, 22);
            this.tbRuolo.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-3, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "RUOLO";
            // 
            // pnlVittima
            // 
            this.pnlVittima.Controls.Add(this.cbLuogoMorte);
            this.pnlVittima.Controls.Add(this.cbcLuogoNascita);
            this.pnlVittima.Controls.Add(this.label16);
            this.pnlVittima.Controls.Add(this.mtbDataMorte);
            this.pnlVittima.Controls.Add(this.mtbDataNascita);
            this.pnlVittima.Controls.Add(this.mtbOraMorte);
            this.pnlVittima.Controls.Add(this.label12);
            this.pnlVittima.Controls.Add(this.label13);
            this.pnlVittima.Controls.Add(this.label14);
            this.pnlVittima.Controls.Add(this.label15);
            this.pnlVittima.Location = new System.Drawing.Point(216, 148);
            this.pnlVittima.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlVittima.Name = "pnlVittima";
            this.pnlVittima.Size = new System.Drawing.Size(332, 210);
            this.pnlVittima.TabIndex = 72;
            // 
            // cbLuogoMorte
            // 
            this.cbLuogoMorte.FormattingEnabled = true;
            this.cbLuogoMorte.Location = new System.Drawing.Point(147, 135);
            this.cbLuogoMorte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbLuogoMorte.Name = "cbLuogoMorte";
            this.cbLuogoMorte.Size = new System.Drawing.Size(132, 24);
            this.cbLuogoMorte.TabIndex = 75;
            // 
            // cbcLuogoNascita
            // 
            this.cbcLuogoNascita.FormattingEnabled = true;
            this.cbcLuogoNascita.Location = new System.Drawing.Point(147, 34);
            this.cbcLuogoNascita.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbcLuogoNascita.Name = "cbcLuogoNascita";
            this.cbcLuogoNascita.Size = new System.Drawing.Size(132, 24);
            this.cbcLuogoNascita.TabIndex = 74;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(-3, 135);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(113, 17);
            this.label16.TabIndex = 59;
            this.label16.Text = "LUOGO MORTE";
            // 
            // mtbDataMorte
            // 
            this.mtbDataMorte.Location = new System.Drawing.Point(147, 68);
            this.mtbDataMorte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbDataMorte.Mask = "00/00/0000";
            this.mtbDataMorte.Name = "mtbDataMorte";
            this.mtbDataMorte.Size = new System.Drawing.Size(132, 22);
            this.mtbDataMorte.TabIndex = 58;
            this.mtbDataMorte.ValidatingType = typeof(System.DateTime);
            // 
            // mtbDataNascita
            // 
            this.mtbDataNascita.Location = new System.Drawing.Point(148, 4);
            this.mtbDataNascita.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbDataNascita.Mask = "00/00/0000";
            this.mtbDataNascita.Name = "mtbDataNascita";
            this.mtbDataNascita.Size = new System.Drawing.Size(132, 22);
            this.mtbDataNascita.TabIndex = 57;
            this.mtbDataNascita.ValidatingType = typeof(System.DateTime);
            // 
            // mtbOraMorte
            // 
            this.mtbOraMorte.Location = new System.Drawing.Point(147, 100);
            this.mtbOraMorte.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbOraMorte.Mask = "00:00";
            this.mtbOraMorte.Name = "mtbOraMorte";
            this.mtbOraMorte.Size = new System.Drawing.Size(132, 22);
            this.mtbOraMorte.TabIndex = 56;
            this.mtbOraMorte.ValidatingType = typeof(System.DateTime);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(-3, 39);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 17);
            this.label12.TabIndex = 54;
            this.label12.Text = "LUOGO DI NASCITA";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(-4, 103);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 17);
            this.label13.TabIndex = 53;
            this.label13.Text = "ORA MORTE";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(-4, 71);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 17);
            this.label14.TabIndex = 52;
            this.label14.Text = "DATA MORTE";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(-3, 11);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 17);
            this.label15.TabIndex = 51;
            this.label15.Text = "DATA DI NASCITA";
            // 
            // cbSesso
            // 
            this.cbSesso.FormattingEnabled = true;
            this.cbSesso.Location = new System.Drawing.Point(364, 116);
            this.cbSesso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbSesso.Name = "cbSesso";
            this.cbSesso.Size = new System.Drawing.Size(132, 24);
            this.cbSesso.TabIndex = 71;
            // 
            // FrmIndividuo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 523);
            this.Controls.Add(this.cbSesso);
            this.Controls.Add(this.pnlCapo);
            this.Controls.Add(this.pnlVittima);
            this.Controls.Add(this.pnlAssassino);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btSalva);
            this.Controls.Add(this.btAnnulla);
            this.Controls.Add(this.tbCognome);
            this.Controls.Add(this.tbNome);
            this.Controls.Add(this.tbCodiceFiscale);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmIndividuo";
            this.Text = "FrmIndividuo";
            this.Load += new System.EventHandler(this.FrmIndividuo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlAssassino.ResumeLayout(false);
            this.pnlAssassino.PerformLayout();
            this.pnlCapo.ResumeLayout(false);
            this.pnlCapo.PerformLayout();
            this.pnlVittima.ResumeLayout(false);
            this.pnlVittima.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox mtbGiornoArresto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.TextBox tbNumAnniGalera;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMovente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbCognome;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TextBox tbCodiceFiscale;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlAssassino;
        private System.Windows.Forms.Panel pnlCapo;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbRuolo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlVittima;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox mtbDataMorte;
        private System.Windows.Forms.MaskedTextBox mtbDataNascita;
        private System.Windows.Forms.MaskedTextBox mtbOraMorte;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbCittaNascita;
        private System.Windows.Forms.ComboBox cbSesso;
        private System.Windows.Forms.ComboBox cbLuogoMorte;
        private System.Windows.Forms.ComboBox cbcLuogoNascita;
    }
}