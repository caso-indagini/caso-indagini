﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmProve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlProva = new System.Windows.Forms.Panel();
            this.tbTipologia = new System.Windows.Forms.TextBox();
            this.btSalva = new System.Windows.Forms.Button();
            this.btAnnulla = new System.Windows.Forms.Button();
            this.tbDescrizione = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btModificaProva = new System.Windows.Forms.Button();
            this.btEliminaProva = new System.Windows.Forms.Button();
            this.btNuovaProva = new System.Windows.Forms.Button();
            this.pnlProva.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(16, 15);
            this.listView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(507, 377);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "TIPOLOGIA";
            this.columnHeader1.Width = 105;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "DESCRIZIONE";
            this.columnHeader2.Width = 266;
            // 
            // pnlProva
            // 
            this.pnlProva.Controls.Add(this.tbTipologia);
            this.pnlProva.Controls.Add(this.btSalva);
            this.pnlProva.Controls.Add(this.btAnnulla);
            this.pnlProva.Controls.Add(this.tbDescrizione);
            this.pnlProva.Controls.Add(this.label2);
            this.pnlProva.Controls.Add(this.label1);
            this.pnlProva.Location = new System.Drawing.Point(671, 15);
            this.pnlProva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlProva.Name = "pnlProva";
            this.pnlProva.Size = new System.Drawing.Size(312, 384);
            this.pnlProva.TabIndex = 4;
            // 
            // tbTipologia
            // 
            this.tbTipologia.Location = new System.Drawing.Point(120, 10);
            this.tbTipologia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTipologia.Multiline = true;
            this.tbTipologia.Name = "tbTipologia";
            this.tbTipologia.Size = new System.Drawing.Size(155, 24);
            this.tbTipologia.TabIndex = 36;
            // 
            // btSalva
            // 
            this.btSalva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btSalva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btSalva.Location = new System.Drawing.Point(173, 315);
            this.btSalva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btSalva.Name = "btSalva";
            this.btSalva.Size = new System.Drawing.Size(103, 49);
            this.btSalva.TabIndex = 35;
            this.btSalva.Text = "SALVA";
            this.btSalva.UseVisualStyleBackColor = false;
            this.btSalva.Click += new System.EventHandler(this.btSalva_Click);
            // 
            // btAnnulla
            // 
            this.btAnnulla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btAnnulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.btAnnulla.Location = new System.Drawing.Point(27, 315);
            this.btAnnulla.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btAnnulla.Name = "btAnnulla";
            this.btAnnulla.Size = new System.Drawing.Size(116, 49);
            this.btAnnulla.TabIndex = 34;
            this.btAnnulla.Text = "ANNULLA";
            this.btAnnulla.UseVisualStyleBackColor = false;
            this.btAnnulla.Click += new System.EventHandler(this.btAnnulla_Click);
            // 
            // tbDescrizione
            // 
            this.tbDescrizione.Location = new System.Drawing.Point(120, 53);
            this.tbDescrizione.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbDescrizione.Multiline = true;
            this.tbDescrizione.Name = "tbDescrizione";
            this.tbDescrizione.Size = new System.Drawing.Size(155, 24);
            this.tbDescrizione.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 32;
            this.label2.Text = "DESCRIZIONE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 30;
            this.label1.Text = "TIPOLOGIA";
            // 
            // btModificaProva
            // 
            this.btModificaProva.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.penna;
            this.btModificaProva.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btModificaProva.Location = new System.Drawing.Point(532, 107);
            this.btModificaProva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btModificaProva.Name = "btModificaProva";
            this.btModificaProva.Size = new System.Drawing.Size(85, 81);
            this.btModificaProva.TabIndex = 38;
            this.btModificaProva.UseVisualStyleBackColor = true;
            // 
            // btEliminaProva
            // 
            this.btEliminaProva.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.cestino;
            this.btEliminaProva.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btEliminaProva.Location = new System.Drawing.Point(532, 196);
            this.btEliminaProva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btEliminaProva.Name = "btEliminaProva";
            this.btEliminaProva.Size = new System.Drawing.Size(85, 81);
            this.btEliminaProva.TabIndex = 37;
            this.btEliminaProva.UseVisualStyleBackColor = true;
            this.btEliminaProva.Click += new System.EventHandler(this.btEliminaProva_Click);
            // 
            // btNuovaProva
            // 
            this.btNuovaProva.BackColor = System.Drawing.Color.Transparent;
            this.btNuovaProva.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.inserisciDEF;
            this.btNuovaProva.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btNuovaProva.Location = new System.Drawing.Point(532, 18);
            this.btNuovaProva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btNuovaProva.Name = "btNuovaProva";
            this.btNuovaProva.Size = new System.Drawing.Size(85, 81);
            this.btNuovaProva.TabIndex = 36;
            this.btNuovaProva.UseVisualStyleBackColor = false;
            this.btNuovaProva.Click += new System.EventHandler(this.btNuovaProva_Click);
            // 
            // FrmProve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 414);
            this.Controls.Add(this.btModificaProva);
            this.Controls.Add(this.btEliminaProva);
            this.Controls.Add(this.pnlProva);
            this.Controls.Add(this.btNuovaProva);
            this.Controls.Add(this.listView1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmProve";
            this.Text = "FrmProve";
            this.pnlProva.ResumeLayout(false);
            this.pnlProva.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Panel pnlProva;
        private System.Windows.Forms.Button btSalva;
        private System.Windows.Forms.Button btAnnulla;
        private System.Windows.Forms.TextBox tbDescrizione;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btModificaProva;
        private System.Windows.Forms.Button btEliminaProva;
        private System.Windows.Forms.Button btNuovaProva;
        private System.Windows.Forms.TextBox tbTipologia;
    }
}