﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void btNuovoCaso_Click(object sender, EventArgs e)
        {

            FrmCaso frmCaso = new FrmCaso();
            frmCaso.ShowDialog();
        }

        private void btCasiVecchi_Click(object sender, EventArgs e)
        {
            FrmCasi frmCasi = new FrmCasi();
            frmCasi.ShowDialog();
        }

        private void btEsci_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btLogOut_Click(object sender, EventArgs e)
        {
            FrmAccesso frmAccesso = new FrmAccesso();
            frmAccesso.ShowDialog();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmAccesso frmAccesso = new FrmAccesso();
            frmAccesso.ShowDialog();
        }
    }
}
