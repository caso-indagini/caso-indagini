﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmCasi : Form
    {
        public FrmCasi()
        {
            InitializeComponent();
        }

        private void btNuovoCaso_Click(object sender, EventArgs e)
        {

        }

        private void FrmCasi_Load(object sender, EventArgs e)
        {
            popolaCombobox();//traino
        }
        private void popolaCombobox()//traino
        {
            foreach (Program.eTipologiaCaso valore in Enum.GetValues(typeof(Program.eTipologiaCaso)))
            {
                cbRicercaTipologia.Items.Add(valore.ToString());
            }
        }

    }
}
