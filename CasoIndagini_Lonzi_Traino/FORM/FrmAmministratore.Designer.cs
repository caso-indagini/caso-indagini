﻿namespace CasoIndagini_Lonzi_Traino
{
    partial class FrmAmministratore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btPassaCaso = new System.Windows.Forms.Button();
            this.btCapoIndagine = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 41);
            this.label1.TabIndex = 2;
            this.label1.Text = "INSERISCI CAPO D\'INDAGINE";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btPassaCaso
            // 
            this.btPassaCaso.BackColor = System.Drawing.Color.Transparent;
            this.btPassaCaso.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.file;
            this.btPassaCaso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btPassaCaso.Location = new System.Drawing.Point(321, 79);
            this.btPassaCaso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPassaCaso.Name = "btPassaCaso";
            this.btPassaCaso.Size = new System.Drawing.Size(127, 119);
            this.btPassaCaso.TabIndex = 1;
            this.btPassaCaso.UseVisualStyleBackColor = false;
            // 
            // btCapoIndagine
            // 
            this.btCapoIndagine.BackColor = System.Drawing.Color.Transparent;
            this.btCapoIndagine.BackgroundImage = global::CasoIndagini_Lonzi_Traino.Properties.Resources.uomo;
            this.btCapoIndagine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btCapoIndagine.Location = new System.Drawing.Point(67, 79);
            this.btCapoIndagine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btCapoIndagine.Name = "btCapoIndagine";
            this.btCapoIndagine.Size = new System.Drawing.Size(127, 119);
            this.btCapoIndagine.TabIndex = 0;
            this.btCapoIndagine.UseVisualStyleBackColor = false;
            this.btCapoIndagine.Click += new System.EventHandler(this.btCapoIndagine_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(297, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "PASSA CASO";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FrmAmministratore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(517, 224);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btPassaCaso);
            this.Controls.Add(this.btCapoIndagine);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmAmministratore";
            this.Text = "FrmAmministratore";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btCapoIndagine;
        private System.Windows.Forms.Button btPassaCaso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}