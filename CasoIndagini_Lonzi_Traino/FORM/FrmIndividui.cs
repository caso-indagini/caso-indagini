﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public partial class FrmIndividui : Form
    {
        bool _assassino, _vittima, _capo;
       

        public FrmIndividui(bool capo, bool assassino, bool vittima)
        {
            InitializeComponent();
            _assassino = assassino;
            _vittima = vittima;
            _capo = capo;
        }

        private void btNuovo_Click(object sender, EventArgs e)
        {
            if(pnlAssassino.Visible)
            {
                FrmIndividuo frmAssassino = new FrmIndividuo(true, false, false);
                frmAssassino.ShowDialog();
            }
            else if(pnlCapi.Visible)
            {
                FrmIndividuo frmCapo = new FrmIndividuo(false, false,true);
                frmCapo.ShowDialog();
            }
            else if (pnlVittime.Visible)
            {
                FrmIndividuo frmVittima = new FrmIndividuo(false, true, false);
                frmVittima.ShowDialog();
            }
        }


        private void btModifica_Click(object sender, EventArgs e)
        {
            if (pnlAssassino.Visible)
            {
                FrmIndividuo frmAssassino = new FrmIndividuo(true, false, false);
                frmAssassino.ShowDialog();
            }
            else if (pnlCapi.Visible)
            {
                FrmIndividuo frmCapo = new FrmIndividuo(false, false, true);
                frmCapo.ShowDialog();
            }
            else if (pnlVittime.Visible)
            {
                FrmIndividuo frmVittima = new FrmIndividuo(false, true, false);
                frmVittima.ShowDialog();
            }
        }

        private void btElimina_Click(object sender, EventArgs e)
        {
            DBManager dbm = new DBManager();

            string errore = "";

            string sql = "DELETE INTO persone (Cf, nome, cognome, sesso) values (@Cf, @nome, @cognome, @sesso)";
        }

        private void FrmIndividui_Load(object sender, EventArgs e)
        {
            if (_assassino)
            {
                pnlVittime.Visible = false;
                pnlCapi.Visible = false;
            }
            else if (_vittima)
            {
                pnlAssassino.Visible = false;
                pnlCapi.Visible = false;
            }
            else if (_capo)
            {
                pnlVittime.Visible = false;
                pnlAssassino.Visible = false;
            }
            PopolaCB();
        }
        void PopolaCB()
        {
            //foreach (ClsCaso.eTipologia valore in Enum.GetValues(typeof(ClsCaso.eTipologia)))
            //{
            //    cbTipologia.Items.Add(valore.ToString());
            //}
        }
    }
}
