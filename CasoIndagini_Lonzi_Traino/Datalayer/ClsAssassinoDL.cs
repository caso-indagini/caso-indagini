﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    //TRAINO SABRINA
    class ClsAssassinoDL :ClsPersona
    {
        //ATTRIBUTI
        public string movente;
        public DateTime giornoArresto;
        public int numAnniPrigione;
        public string tipologiaAssassinio;

        //PROPRIETA'
        public string Movente { get => movente; set => movente = value; }
        public DateTime GiornoArresto { get => giornoArresto; set => giornoArresto = value; }
        public int NumAnniPrigione { get => numAnniPrigione; set => numAnniPrigione = value; }
        public string TipologiaAssassinio { get => tipologiaAssassinio; set => tipologiaAssassinio = value; }

        //COSTRUTTORE
        public ClsAssassinoDL()
        {

        }
    }
}
