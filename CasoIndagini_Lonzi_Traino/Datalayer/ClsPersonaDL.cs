﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    // Martina Lonzi   
    public class ClsPersona
    {
        // enumeratore
        public enum eSesso
        {
            Maschio,
            Femmina
        }

        // attributi
        private string cognome;
        private eSesso sesso;
        private string codiceFiscale;
        private string nome;

        // proprietà
        public string CodiceFiscale { get => codiceFiscale; set => codiceFiscale = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Cognome { get => cognome; set => cognome = value; }
        public eSesso Sesso { get => sesso; set => sesso = value; }

        // costruttore
        public ClsPersona()
        {

        }




    }
}
