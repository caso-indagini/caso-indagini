using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    // Lonzi Martina
    public class ClsCasoDL
    {
        #region ATTRIBUTI
        // attributi
        DateTime _oraRitrovamento, _dataRitrovamento;
        int _ID;
        Program.eTipologiaCaso _Tipologia;
        string _cfCapoIndagini, _cfAssassino;
        Program.eCitta _cittaRitrovamento;
        #endregion

        #region PROPRIETA'
        // proprietà
        public DateTime OraRitrovamento { get => _oraRitrovamento; set => _oraRitrovamento = value; }
        public DateTime DataRitrovamento { get => _dataRitrovamento; set => _dataRitrovamento = value; }
        public int ID { get => _ID; set => _ID = value; }
        public Program.eTipologiaCaso Tipologia { get => _Tipologia; set => _Tipologia = value; }
        public string CfCapoIndagini { get => _cfCapoIndagini; set => _cfCapoIndagini = value; }
        public string CfCapoIndagini1 { get => _cfCapoIndagini; set => _cfCapoIndagini = value; }
        public Program.eCitta CittaRitrovamento { get => _cittaRitrovamento; set => _cittaRitrovamento = value; }
        #endregion

        #region COSTRUTTORI
        // costruttore
        public ClsCasoDL()
        {

        }
        public ClsCasoDL(int  id, Program.eTipologiaCaso tipologia, DateTime dataRitrovamento, DateTime oraRitrovamento, string cfCapoIndagini, string cfAssassino, Program.eCitta cittaRitrovamento)
        {
            _ID = id;
            _Tipologia = tipologia;
            _dataRitrovamento = dataRitrovamento;
            _oraRitrovamento = oraRitrovamento;
            _cfCapoIndagini = cfCapoIndagini;
            _cfAssassino = cfAssassino;
            _cittaRitrovamento = cittaRitrovamento;
        }
        #endregion

    }
}
