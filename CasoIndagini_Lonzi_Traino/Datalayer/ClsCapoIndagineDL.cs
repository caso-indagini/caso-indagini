﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    class ClsCapoIndagineDL:ClsPersona
    {
        //ATTRIBUTI
        private string username;
        private string password;
        private string ruolo;

        //PROPRIETA'
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Ruolo { get => ruolo; set => ruolo = value; }

        //COSTRUTTORE
        public ClsCapoIndagineDL()
        {

        }
    }
}
