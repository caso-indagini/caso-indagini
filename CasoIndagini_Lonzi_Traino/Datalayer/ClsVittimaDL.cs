﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    //TRAINO SABRINA
    class ClsVittima :ClsPersona
    {
        //ATTRIBUTI
        private DateTime dataNascita;
        private DateTime dataMorte;
        public DateTime oraMorte;
        private string tipologiaMorte;
        //PROPRIETA'
        public DateTime DataNascita { get => dataNascita; set => dataNascita = value; }
        public DateTime DataMorte
        {
            get
            {
                return dataMorte;
            }
            set
            {
                if (value < dataNascita)
                    dataMorte = dataNascita.AddDays(+1);
                else
                    dataMorte = value;
            }
        }
        public DateTime OraMorte
        {
            get
            {
                return oraMorte;
            }
            set
            {
                if (value < dataNascita)
                    oraMorte = dataNascita.AddHours(+1);
                else
                    oraMorte = value;
            }
        }

        public string TipologiaMorte { get => tipologiaMorte; set => tipologiaMorte = value; }

        //COSTRUTTORE
        public ClsVittima()
        {

        }
        
    }
}
