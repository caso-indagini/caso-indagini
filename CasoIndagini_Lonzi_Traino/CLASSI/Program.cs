﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    public static class Program
    {
        // enumeratore per il sesso della persona lonzi
        public enum eSesso
        {
            M,
            F
        }
        // enumeratore per il le città della persona lonzi
        public enum eCitta
        {
            Roma,
            Milano,
            Firenze
        }
        // enumeratore per la tipologia di caso lonzi
        public enum eTipologiaCaso
        {
            OMICIDIO,
            SUICIDIO,
            INCENDIO_DOLOSO,
            ALTRO, 
            NON_RILEVATO
        }

        
        //public static List<ClsCasoDL> _listaCasi = new List<ClsCasoDL>();
        //public static List<ClsCapoIndagineDL> _listaCapiIndagine = new List<ClsCapoIndagineDL>();
        //public static List<ClsVittima> _listaVittime = new List<ClsVittima>();
        //public static List<ClsAssassinoDL> _listaAssassini = new List<ClsAssassinoDL>();
        //public static List<ClsProva> _listaProve = new List<ClsProva>();
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }
    }
}
