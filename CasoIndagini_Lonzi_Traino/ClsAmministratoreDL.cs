﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    // Martina Lonzi
    public class ClsAmministratoreDL : ClsPersona
    {
        // attributi
        private string password;
        private string username;

        //proprietà
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }

        // costruttore
        public ClsAmministratoreDL()
        {

        }

       
    }
}
