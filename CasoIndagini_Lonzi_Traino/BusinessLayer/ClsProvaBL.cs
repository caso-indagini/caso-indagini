﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    static class ClsProvaBL
    {
        #region METODI
        static bool ModificaProva(ClsProva prova, List<ClsProva> listaProve, int indice)
        {
            listaProve[indice] = prova;
            return true;
        }

        static bool EliminaProva(ClsProva ProvadaEliminare, List<ClsProva> listaCasi)
        {
            listaCasi.Remove(ProvadaEliminare);
            return true;
        }
        #endregion
    }
}
