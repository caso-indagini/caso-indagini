﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    //TRAINO SABRINA
    static class ClsAssassinoBL
    {
        static public bool ModificaAssassino(ClsAssassinoDL assassino, List<ClsAssassinoDL> lista, int indice)
        {
            lista[indice] = assassino;
            return true;
        }
        static public bool EliminaAssassino(ClsAssassinoDL assassinoDaEliminare, List<ClsAssassinoDL> lista)
        {
            lista.Remove(assassinoDaEliminare);
            return true;
        }
        static public  List<ClsAssassinoDL> RiecercaListaAssassini(string Tipologia, List<ClsAssassinoDL> lista)
        {
            List<ClsAssassinoDL> listVittimaFiltrata = new List<ClsAssassinoDL>();
            listVittimaFiltrata = lista.FindAll(_app => _app.TipologiaAssassinio == Tipologia);
            return listVittimaFiltrata;
        }
    }
}
