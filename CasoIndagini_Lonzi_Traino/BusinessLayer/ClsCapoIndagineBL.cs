﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySqlConnector;
using System.Data;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    
    static class ClsCapoIndagineBL
    {
        //METODI
        static public void cercaCapo(string Username, string Password)
        {
            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT * FROM logincapi WHERE username=@username AND psw=@psw";

            MySqlParameter[] parametri =
            {
                 new MySqlParameter("@username", Username),
                 new MySqlParameter("@psw",Password)
            };

            DataTable dt = dbm.GetDataTableByQuery(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
            {
                FrmMain frmMain = new FrmMain();
                frmMain.ShowDialog();
            }
            else
            {
                MessageBox.Show($"Errore:{errore}");
            }

        } //lonzi
    }
}
