﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySqlConnector;
using System.Data;
using System.Windows.Forms;

namespace CasoIndagini_Lonzi_Traino
{
    // lonzi
    internal class ClsCasoBL
    {
        #region ATTRIBUTI
        DBManager _dbm = null;
        #endregion

        #region COSTRUTTORI
        public ClsCasoBL(DBManager dbm)
        {
            _dbm = dbm;
        }
        #endregion

        #region Metodi

        // lista dei casi lonzi
        public List<ClsCasoDL> GetCasi(ref string errore)
        {
            DataTable dt = null;
            List<ClsCasoDL> _listaCasi = new List<ClsCasoDL>();

            // 1. Sfruttando la classe DbManager
            try
            {
                string query = "SELECT * FROM casi"; //query veloce!
                dt = _dbm.GetDataTableByQuery(query, null, ref errore);
                if (string.IsNullOrEmpty(errore))
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Potrei scrivere anche su ua sola riga ma così è più leggibile
                        ClsCasoDL _libro = new ClsCasoDL(
                            (int)dt.Rows[i]["Id"],
                            (Program.eTipologiaCaso)dt.Rows[i]["Tipologia"],
                            (DateTime)dt.Rows[i]["dataRitrovamento"],
                             (DateTime)dt.Rows[i]["oraRitrovamento"],
                            dt.Rows[i]["cfCapoIndagini"].ToString(),
                            dt.Rows[i]["cfAssassino"].ToString(),
                           (Program.eCitta)dt.Rows[i]["cittaRitrovamento"]);
                        _listaCasi.Add(_libro);
                    }
                }
            }
            catch (Exception ex)
            {
                errore = ex.Message;
            }

            

            return _listaCasi;

        }
        // verificare se l'id che inserisce 
        private void Cerca(int id)
        {

            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT * FROM libri WHERE Id=@id";

            MySqlParameter[] parametri =
            {
                 new MySqlParameter("@id", id),
                 
            };

            DataTable dt = dbm.GetDataTableByQuery(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
            {
                MessageBox.Show("inserisci un'altro id");
            }
            else
            {
               
           
            }


        }
        #endregion
    }






        //static bool ModificaCaso(ClsCaso caso, List<ClsCaso> listaCasi, int indice)
        //{
        //    listaCasi[indice] = caso;
        //    return true;
        //}

        //static bool Elimina(ClsCaso casoDaEliminare, List<ClsCaso> listaCasi)
        //{
        //    listaCasi.Remove(casoDaEliminare);
        //    return true;
        //}

        //static List<ClsCaso> RicercaCasi(string Tipologia, List<ClsCaso> casi)
        //{
        //    List<ClsCaso> listaCasiFiltrata = new List<ClsCaso>();
        //    //listaCasiFiltrata = casi.FindAll(_app => _app._tipologia.ToString() == Tipologia);
        //    return listaCasiFiltrata;
        //}
        //#endregion
    //}
}
