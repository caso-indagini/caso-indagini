﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySqlConnector;
using System.Data;

namespace CasoIndagini_Lonzi_Traino
{
    static class ClsAmministratoreBL
    {
        #region Metodi
        //cerca se la password è nella tabella Ammministratori nel database, censiti da database
        static public void cercaAmministratore(string Username, string Password)//lonzi
        {

            DBManager dbm = new DBManager();
            string errore = "";

            string sql = "SELECT * FROM amministratori WHERE username=@username AND psw=@psw";

            MySqlParameter[] parametri =
            {
                 new MySqlParameter("@username", Username),
                 new MySqlParameter("@psw",Password)
            };

            DataTable dt = dbm.GetDataTableByQuery(sql, parametri, ref errore);

            if (string.IsNullOrEmpty(errore))
            {
                FrmAmministratore frmAmministratore = new FrmAmministratore();
                frmAmministratore.ShowDialog();
            }
            else
            {
               ClsCapoIndagineBL.cercaCapo(Username, Password);
            }


        }
        #endregion
    }
}
