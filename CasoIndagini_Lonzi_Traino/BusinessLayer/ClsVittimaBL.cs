﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    static class ClsVittimaBL
    {
        #region METODI
        //METODI
        static bool ModificaVittima(ClsVittima vittima, List<ClsVittima> lista, int indice)
        {
            lista[indice] = vittima;
            return true;
        }
        static bool EliminaVittima(ClsVittima vittimaDaEliminare, List<ClsVittima> lista)
        {
            lista.Remove(vittimaDaEliminare);
            return true;
        }
        static List<ClsVittima> RicercaListaVittime(string Tipologia, List<ClsVittima> lista)
        {
            List<ClsVittima> listVittimaFiltrata = new List<ClsVittima>();
            listVittimaFiltrata = lista.FindAll(_app => _app.TipologiaMorte == Tipologia);
            return listVittimaFiltrata;
        }
        #endregion
    }
}
