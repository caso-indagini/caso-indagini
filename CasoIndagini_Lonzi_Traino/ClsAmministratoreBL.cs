﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    static class ClsAmministratoreBL
    {
        #region Metodi
        static public bool ControllaUtente(string passwordInserita, string UtenteInserito, List<ClsCapoIndagineDL> listaCapi)
        {
            bool corretto = false;
            for (int i = 0; i < listaCapi.Count; i++)
            {
                if (listaCapi[i].Password == passwordInserita && listaCapi[i].Username == UtenteInserito)
                    corretto = true;
            }
            return corretto;
        }
        #endregion
    }
}
