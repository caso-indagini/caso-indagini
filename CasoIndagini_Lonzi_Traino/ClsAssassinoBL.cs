﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasoIndagini_Lonzi_Traino
{
    static class ClsAssassinoBL
    {
        #region METODI
        //METODI
        static bool ModificaAssassino(ClsAssassinoDL assassino, List<ClsAssassinoDL> lista, int indice)
        {
            lista[indice] = assassino;
            return true;
        }
        static bool EliminaVittima(ClsAssassinoDL assassinoDaEliminare, List<ClsAssassinoDL> lista)
        {
            lista.Remove(assassinoDaEliminare);
            return true;
        }
        static List<ClsAssassinoDL> RiecercaListaAssassini(string Tipologia, List<ClsAssassinoDL> lista)
        {
            List<ClsAssassinoDL> listaAssasinoFiltrata = new List<ClsAssassinoDL>();
            listaAssasinoFiltrata = lista.FindAll(_app => _app.TipologiaAssassinio == Tipologia);
            return listaAssasinoFiltrata;
        }
        #endregion
    }
}
